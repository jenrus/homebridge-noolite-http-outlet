# homebridge-noolite-http-outlet

`homebridge-noolite-http-outlet` is a plugin for Homebridge.

Control your `noolite-http`-based outlet via Homebridge!

## Installation

If you are new to Homebridge, please first read the Homebridge [documentation](https://www.npmjs.com/package/homebridge).
If you are running on a Raspberry, you will find a tutorial in the [homebridge-punt Wiki](https://github.com/cflurin/homebridge-punt/wiki/Running-Homebridge-on-a-Raspberry-Pi).

Install homebridge:
```sh
sudo npm install -g homebridge
```
Install homebridge-noolite-http-outlet:
```sh
sudo npm install -g homebridge-noolite-http-outlet
```

## Configuration

Add the accessory in `config.json` in your home directory inside `.homebridge`.

```js
   {
      "accessory": "NooLite-HTTP-Outlet",
  		"name": "Розетка",
  		"on_url": "http://<your_ip>/api.htm?ch=<OutletChNumber>&cmd=2",
  		"off_url": "http://<your_ip>/api.htm?ch=<OutletChNumber>&cmd=0",
  		"status_url": "http://<your_ip>/api.htm?ch=<OutletChNumber>",
  		"http_method": "PUT"
    }
```

You can omit `http_method`, it defaults to `POST`.

## Note

OutletInUse is not supported by noolite power units so it is always 0

Feel free to contribute to make this a better plugin!
