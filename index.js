var request = require("request");
var Service, Characteristic;

module.exports = function(homebridge) {
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;

    homebridge.registerAccessory("homebridge-noolite-http-outlet", "NooLite-HTTP-Outlet", NooLiteHTTPOutletAccessory);
}

function NooLiteHTTPOutletAccessory(log, config) {
    // global vars
    this.log = log;

    // configuration vars
    this.name = config["name"];
    this.onURL = config["on_url"];
    this.offURL = config["off_url"];
    this.statusURL = config["status_url"];
    this.httpMethod = config["http_method"] || "POST";

    // state vars
    this.currentStatus = 0; // last known status of the Outlets, off by default

    // register the service and provide the functions
    this.service = new Service.Outlet(this.name);
    // the current state (on / off)
    // https://github.com/KhaosT/HAP-NodeJS/blob/master/lib/gen/HomeKitTypes.js#L1143
    this.service
        .getCharacteristic(Characteristic.On)
        .on('get', this.getOn.bind(this))
        .on('set', this.setOn.bind(this));

    /*Power consumption in Watt*/
    // https://github.com/KhaosT/HAP-NodeJS/blob/master/lib/gen/HomeKitTypes.js#L1177
    this.service
        .getCharacteristic(Characteristic.OutletInUse)
        .on('get', this.getOutletInUse.bind(this));
}

NooLiteHTTPOutletAccessory.prototype.getOn = function(callback) {
    this.log("Requested On %s", this.currentStatus);
    callback(null, this.currentStatus);
}

//Always 0
NooLiteHTTPOutletAccessory.prototype.getOutletInUse = function(callback) {
    this.log("Requested Outlet In use: %s", 0);
    callback(null, 0);
}

NooLiteHTTPOutletAccessory.prototype.setOn = function(state, callback) {
    this.log("Requested Outlet to turn %s", (state ? "on" : "off"));
    /* Not supported by current (09.10.16) noolite version
    this.currentStatus = this.service.getCharacteristic(Characteristic.On);
    if (this.currentStatus == state)
    {
        this.log("Already turned %s", (state ? "on" : "off"));
        callback(null);
        return;
    }*/

    this.httpRequest((state ? this.onURL : this.offURL), this.httpMethod, function() {
        this.log("Success switched %s", (state ? "on" : "off"))
        this.currentStatus = state;

        callback(null);
    }.bind(this));
}

NooLiteHTTPOutletAccessory.prototype.httpRequest = function(url, method, callback) {
  request({
    method: method,
    url: url,
  }, function(err, response, body) {
    if (!err && response.statusCode == 200) {
      callback(null);
    } else {
      this.log("Error getting state (status code %s): %s", response.statusCode, err);
      callback(err);
    }
  }.bind(this));
}

NooLiteHTTPOutletAccessory.prototype.getServices = function() {
  return [this.service];
}
